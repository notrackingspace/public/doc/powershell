# Powershell Sources

* [Getting Started Quickly With PowerShell Logging \| DataSet](https://www.dataset.com/blog/getting-started-quickly-powershell-logging/ "Getting Started Quickly With PowerShell Logging | DataSet")
* [Logging Best Practices\: The 13 You Should Know \| DataSet](https://www.dataset.com/blog/the-10-commandments-of-logging/ "Logging Best Practices: The 13 You Should Know | DataSet")
* [about Logging \- PowerShell \| Microsoft Learn](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_logging?view=powershell-5.1 "about Logging - PowerShell | Microsoft Learn")
